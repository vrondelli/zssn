# ZSSN (Zombie Survival Social Network)

## Problem Description

The world as we know it has fallen into an apocalyptic scenario. A laboratory-made virus is transforming human beings and animals into zombies, hungry for fresh flesh.

I, as a zombie resistance member (and the last survivor who knows how to code), was designated to develop a system to share resources between non-infected humans.

## Running the system
First you have to create mysql database tables using the script in `/data/seed.sql`.
Second install packages via package manager, `yarn` or `npm install`

Then to get the system running you just have to be in root of the project and run `yarn dev` or `npm run dev` all api resources will be at `http://localhost:8000/`.

## Documentation

Documentation via swagger will be at `http://localhost:8000/documentation`