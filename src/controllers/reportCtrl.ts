import * as Hapi from 'hapi';
import * as Joi from 'joi';
import * as Boom from 'boom';

import { inject } from 'inversify';
import { provide } from '@cashfarm/plow';
import { IController } from '@cashfarm/tractor/interfaces';
import { Endpoint } from '@cashfarm/tractor/decorators';
import { Controller, CreatedResponse } from '@cashfarm/tractor';
import { TradeRequest } from '../requests';
import { Guid } from '@cashfarm/lang';
import { TradeService } from '../services';
import { SurvivorStore, ItemStore } from '../readModel';
import { Items } from '../dsl';


@Controller
export class ReportController {
  constructor(
    @inject(SurvivorStore) private survivorStore: SurvivorStore,
    @inject(ItemStore) private itemStore: ItemStore,
    @inject(TradeService) private tradeService: TradeService
  ) {}

  @Endpoint('GET', '/reports/infected', {
    description: 'Returns percentage of infected survivors',
    tags: ['api'],
  })
  public async percentageInfected(req: Hapi.Request, reply: Hapi.ReplyNoContinue) {
    const totalSurvivors = await this.survivorStore.computeAllSurvivors();
    const totalInfected = await this.survivorStore.computeInfectedSurvivors();

    const percentageInfected = Math.floor((totalInfected / totalSurvivors) * 100);

    return reply(`${percentageInfected}% of survivors are infected.`);
  }

  @Endpoint('GET', '/reports/non-infected', {
    description: 'Returns percentage of non-infected survivors',
    tags: ['api'],
  })
  public async percentageNonInfected(req: Hapi.Request, reply: Hapi.ReplyNoContinue) {
    const totalSurvivors = await this.survivorStore.computeAllSurvivors();
    const totalNonInfected = await this.survivorStore.computeNonInfectedSurvivors();

    const percentageNonInfected = Math.floor((totalNonInfected / totalSurvivors) * 100);

    return reply(`${percentageNonInfected}% of survivors are healthy.`);
  }
 
  @Endpoint('GET', '/reports/average-resource', {
    description: 'Returns average amount of each kind of resource by survivor',
    tags: ['api'],
    validate: {
      query: {
        item: Joi.string().only(Items.Water, Items.Food, Items.Medication, Items.Ammunition).required()
      } 
    }
  })
  public async averageResourceBySurvivor(req: Hapi.Request, reply: Hapi.ReplyNoContinue) {
    const totalSurvivors = await this.survivorStore.computeAllSurvivors();
    const totalResource = await this.itemStore.computeItemAmount(req.query.item);

    const averagePerSurvivor = Math.floor(totalResource / totalSurvivors);

    return reply(`${averagePerSurvivor} ${req.query.item}s per survivor.`);
  }

  @Endpoint('GET', '/reports/points-lost', {
    description: 'Returns total points lost because of infected survivor.',
    tags: ['api'],
  })
  public async totalPointsLost(req: Hapi.Request, reply: Hapi.ReplyNoContinue) {
    const totalPointsLost = await this.itemStore.getTotalPointsLost(this.tradeService.ValueMap());

    return reply(`${totalPointsLost} points lost because of infected survivor.`);
  }
}
  
