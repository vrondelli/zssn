import * as Hapi from 'hapi';
import * as Joi from 'joi';
import * as Boom from 'boom';

import { inject } from 'inversify';
import { provide } from '@cashfarm/plow';
import { IController } from '@cashfarm/tractor/interfaces';
import { Endpoint } from '@cashfarm/tractor/decorators';
import { Controller, CreatedResponse } from '@cashfarm/tractor';
import { CreateSurvivorRequest, UpdateLocationRequest } from '../requests';
import { SurvivorStore, SurvivorDto } from '../readModel/'
import { Guid, serialize } from '@cashfarm/lang';
import { Survivor } from '../domain/entities';

@Controller
export class SurvivorController {
  constructor(
    @inject(SurvivorStore) private survivorStore: SurvivorStore,
  ) { }

  @Endpoint('POST', '/survivors', {
    description: 'Creates a survivor',
    tags: ['api'],
    validate: CreateSurvivorRequest.schema
  })
  public create(req: Hapi.Request, reply: Hapi.ReplyNoContinue) {
    const survivorReq = new CreateSurvivorRequest(req);

    console.log(survivorReq);
    const survivor = new Survivor(survivorReq.name, survivorReq.age, survivorReq.gender, survivorReq.location, survivorReq.itens);

    console.log(survivor.inventory.slots);
    this.survivorStore.save(survivor);

    return reply(new CreatedResponse('Survivor Created!', survivor.id.toString()));
  }

  @Endpoint('GET', '/survivors/{id}', {
    description: 'Returns a single survivor',
    tags: ['api'],
    validate: {
      params: {
        id: Joi.string().guid()
      }
    }
  })
  public async getSurvivor(req: Hapi.Request, reply: Hapi.ReplyNoContinue) {
    const survivor = await this.survivorStore.findById(new Guid(req.params.id));

    if (!survivor) {
      reply(Boom.notFound(`Could not found survivor with ${req.params.id} id`))
    }

    return reply(survivor);
  }

  @Endpoint('GET', '/survivors/{id}/flag-infected', {
    description: 'Flag a survivor as infected',
    tags: ['api'],
    validate: {
      params: {
        id: Joi.string().guid()
      },
      query: {
        survivorWhoFlagged: Joi.string().guid().required()
      }
    }
  })
  public async flagSurvivor(req: Hapi.Request, reply: Hapi.ReplyNoContinue) {
    const survivor = await this.survivorStore.findById(new Guid(req.query.id));
    if (!survivor) reply(Boom.notFound(`Could not found survivor with ${req.query.id} id`));

    if (survivor.infected) return reply(Boom.badRequest('Survivor already flagged as infected by 3 other survivors'));

    const flag = survivor.flagAsInfected(new Guid(req.query.survivorWhoFlagged));

    if (!flag) return reply(Boom.badRequest(`Survivor already flagged by ${req.query.survivorWhoFlagged} id`));
     
    this.survivorStore.updateSurvivor(survivor);
    if (flag) return reply(new CreatedResponse('Survivor flagged as infected', survivor.id.toString()));

  }

  @Endpoint('POST', '/survivors/{id}/update-location', {
    description: 'Updates a survivor Location',
    tags: ['api'],
    validate: UpdateLocationRequest.schema
  })
  public async updateLocation(req: Hapi.Request, reply: Hapi.ReplyNoContinue) {
    const survivorReq = new UpdateLocationRequest(req);

    const survivor = await this.survivorStore.findById(survivorReq.id);

    if (!survivor) {
      return Boom.notFound(`Survivor with id ${req.id.toString()} could not be found`);
    }

    survivor.updateLocation(survivorReq.location);
    this.survivorStore.updateSurvivor(survivor);

    return reply(new CreatedResponse('Survivor location updated!', survivor.id.toString()));
  }

}
