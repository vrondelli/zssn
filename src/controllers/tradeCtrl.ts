import * as Hapi from 'hapi';
import * as Joi from 'joi';
import * as Boom from 'boom';

import { inject } from 'inversify';
import { provide } from '@cashfarm/plow';
import { IController } from '@cashfarm/tractor/interfaces';
import { Endpoint } from '@cashfarm/tractor/decorators';
import { Controller, CreatedResponse } from '@cashfarm/tractor';
import { TradeRequest } from '../requests';
import { Guid } from '@cashfarm/lang';
import { TradeService } from '../services';
import { SurvivorStore } from '../readModel';


@Controller
export class TradeController {
  constructor(
    @inject(SurvivorStore) private survivorStore: SurvivorStore,
    @inject(TradeService) private tradeService: TradeService,
  ) {}

  @Endpoint('POST', '/trade', {
    description: 'Creates a trade',
    tags: ['api'],
    validate: TradeRequest.schema
  })
  public async create(req: Hapi.Request, reply: Hapi.ReplyNoContinue) {
    const tradeReq = new TradeRequest(req);

    const firstSurvivor = await this.survivorStore.findById(new Guid(tradeReq.offers[0].survivorId));
		if (!firstSurvivor) return reply(Boom.notFound(`Could not found survivor with ${tradeReq.offers[0].survivorId} id`));
    if (firstSurvivor.infected) reply(Boom.badRequest(`Survivor with ${tradeReq.offers[0].survivorId} id is infected can not trade.`));

		const secondSurvivor = await this.survivorStore.findById(new Guid(tradeReq.offers[1].survivorId));
    if (!secondSurvivor) return reply(Boom.notFound(`Could not found survivor with ${tradeReq.offers[1].survivorId} id`));
    if (secondSurvivor.infected) reply(Boom.badRequest(`Survivor with ${tradeReq.offers[1].survivorId} id is infected can not trade.`));

		const trade = await this.tradeService.trade(firstSurvivor, secondSurvivor,tradeReq.offers);

    return reply(trade);
  }
  
}
