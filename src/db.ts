import { createPool, Pool, PoolConfig } from 'mysql';

const debug = require('debug')('zssn');

const connInfo: PoolConfig = {
  database : process.env.ZSSN_DB_NAME || 'zssn',
  host     : process.env.ZSSN_DB_HOST || 'localhost',
  user     : process.env.ZSSN_DB_USER || 'root',
  password : process.env.ZSSN_DB_PASS || '',
  port     : parseInt(process.env.ZSSN_DB_PORT) || 3306
};

debug('Database connection info', connInfo);

export const ConnectionPool: Pool = createPool(connInfo);
