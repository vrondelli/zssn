import { AggregateRoot } from "@cashfarm/plow";
import { Guid } from "@cashfarm/lang";
import { IItem, OfferItem } from '../../dsl/interfaces';
import { Items } from '../../dsl';
import { InventorySlot } from '../valueObjects/inventorySlot';
import { TradeService } from '../../services';

export class Inventory extends AggregateRoot<Guid> {
  private _survivorId: Guid;
  public get survivorId() {
    return this._survivorId;
  }

  private _slots: InventorySlot[];
  public get slots() {
    return this._slots
  }

  constructor(survivorId: Guid, itens: IItem[], id?: Guid) {
    super(id ? id : new Guid());

    this._survivorId = survivorId;

    this._slots = itens.map(i => {
      const inventorySlot = new InventorySlot(i.name, i.amount);

      return inventorySlot;
    })
  }

  public getItem(item: Items, amount: number) {
    const slotIndex = this._slots.indexOf(this._slots.find(slot => slot.item === item));
    if (slotIndex === -1) return null;

    const slot = this._slots.splice(slotIndex, 1)[0];
    if (slot.amount < amount) return null;

    const newAmount = slot.amount - amount;

    const offerItem: OfferItem = {
      item: slot.item,
      amount: amount
    }

    this._slots.push(new InventorySlot(slot.item, newAmount));

    return offerItem;
  }

  public addItem(item: Items, amount: number) {
    const slotIndex = this._slots.indexOf(this._slots.find(slot => slot.item === item));
   
    if (slotIndex === -1) {
      const inventorySlot = new InventorySlot(item, amount);
      this._slots.push(inventorySlot);

      return true;
    }

    const slot = this._slots.splice(slotIndex, 1)[0];

    const newAmount = slot.amount + amount;

    this._slots.push(new InventorySlot(slot.item, newAmount));

    return true;
  }

  
}