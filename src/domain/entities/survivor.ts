import { Guid, Class } from '@cashfarm/lang';
import { AggregateRoot, DomainException } from "@cashfarm/plow";
import { Genders, Items, TradeStatus } from '../../dsl';
import { Location } from '../valueObjects';
import { InventoryStore } from '../../readModel/inventoryStore';
import { Inventory } from './inventory';
import { IItem, OfferItem, IOffer } from '../../dsl/interfaces';
import { inject } from 'inversify';
import { Offer } from '../valueObjects/offer';
import { TradeResponse } from '../../services';
import * as _ from 'lodash';

export class Survivor extends AggregateRoot<Guid> {
  private _inventoryId: Guid;
  public get inventoryId() {
    return this._inventoryId;
  }
  
  private _name: string;
  public get name() {
    return this._name;
  }

  private _age: number;
  public get age() {
    return this._age;
  }

  private _gender: Genders;
  public get gender() {
    return this._gender
  }

  @Class(() => Location)
  private _LastLocation: Location;
  public get lastLocation() {
    return this._LastLocation;
  }

  private _survivorsWhoFlaggedAsInfected: Guid[];
  public get survivorsWhoFlaggedAsInfected() {
    return this._survivorsWhoFlaggedAsInfected;
  }

  private _infected: boolean;
  public get infected() {
    return this._infected;
  }

  private _inventory: Inventory;
  public get inventory() {
    if (this.infected)
     throw new DomainException(`Survivor with ${this.id.toString()} is infected, cannot acsess inventory`);
    
    return this._inventory;
  }

  private _createdAt: Date;
  public get createdAt() {
    return this._createdAt;
  }

  private _updateAt: Date;
  public get updateAt() {
    return this._updateAt;
  }

  constructor(
    name: string, age: number, gender: Genders,
    lastLocation: Location, itens: IItem[], id?: Guid, inventoryId?: Guid, 
    infected?: boolean, timesFlagged?: Guid[], createdAt?: Date, updatedAt?: Date
  ) {
    const newId = id ? id : new Guid();
    super(newId);


    this._name = name;
    this._age = age;
    this._gender = gender;
    this._LastLocation = lastLocation;
    this._infected = infected ? infected : false;
    this._survivorsWhoFlaggedAsInfected = timesFlagged ? timesFlagged : [];
    this._inventoryId = inventoryId ?  inventoryId : null;
    this._inventory = this._inventoryId ? new Inventory(newId, itens, this._inventoryId) : new Inventory(newId, itens);
    this._inventoryId = this._inventory.id;
    this._createdAt = createdAt ? createdAt : new Date();
    this._updateAt = updatedAt;
  }

  public updateLocation(Location: Location) {
    this._LastLocation = Location;
    this._updateAt = new Date();
  }

  public flagAsInfected(id: Guid): boolean {
    if (this._survivorsWhoFlaggedAsInfected.find(s => s.toString() === id.toString())) 
      return false;

    this._survivorsWhoFlaggedAsInfected.push(id);

    if (this._survivorsWhoFlaggedAsInfected.length >= 3) this.markAsInfected();
    this._updateAt = new Date();

    return true;
  }

  public tradeItens(offerToGet: Offer, offerToAdd: Offer): TradeResponse {
    const items: OfferItem[] = offerToGet.items.map(i => {
      const item = this.inventory.getItem(i.item, i.amount);
      return item;
    });
    if (_.isEqual(offerToGet.items, items)) {
      offerToAdd.items.forEach(i => {
        this.inventory.addItem(i.item, i.amount);
      })
      console.log('Trade completed Successfully!');

      return new TradeResponse(TradeStatus.Success, 'Trade completed Successfully!');
      
    }
    else {
      console.log('Trade failed, check if item exists in inventory or enough amount !');
      return new TradeResponse(TradeStatus.Failed, `Survivor with ${this.id.toString()} does not have item or enough amount`);
    }
  }

  public getInventoryTotalPoints(valueMap: object) {
    const totalSlots = this._inventory.slots.map(s => {
      const slotPoints = s.amount * valueMap[s.item];

      return slotPoints;
    })
    const total = totalSlots.reduce((a, b) => a + b);

    return total;
  }

  private markAsInfected() {
    this._infected = true;
  }
}
