import { ValueObject } from "@cashfarm/plow";

import { Guid } from '@cashfarm/lang';
import { TradeService } from '../../services/tradeService';
import { Items } from '../../dsl';

export class InventorySlot extends ValueObject<InventorySlot> {
    public readonly item: Items;
    public readonly amount: number;

    constructor(item: Items, amount: number) {
        super(InventorySlot, ['item', 'amount']);

        this.item = item;
        this.amount = amount;
    }
}