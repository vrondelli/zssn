import { ValueObject } from "@cashfarm/plow";

export class Location extends ValueObject<Location> {
  public readonly latitude: string;
  public readonly longitude: string;

  constructor(latitude: string, longitude: string) {
    super(Location, ['latitude', 'longitude'])
    
    this.latitude = latitude;
    this.longitude = longitude;
  }
}