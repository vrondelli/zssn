import { ValueObject } from "@cashfarm/plow";

import { Guid } from '@cashfarm/lang';
import { TradeService } from '../../services/tradeService';
import { IOffer, OfferItem } from '../../dsl/interfaces';

export class Offer extends ValueObject<Offer> {

  public readonly items: OfferItem[];
  public readonly totalPoints: number

  constructor(offer: IOffer, valuesMap: object) {
    super(Offer, ['items', 'totalPoints']);
    let total = 0;
    
    this.items = offer.items.map(i => {
      const offerItem: OfferItem = {
        item: i.name,
        amount: i.amount
      };

      total += (i.amount * valuesMap[i.name])

      return offerItem;
    })

    this.totalPoints = total;
  }
}