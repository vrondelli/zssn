export enum Genders {
	Female = 'female',
	Male = 'male'
}

export enum Items {
	Water = 'water',
	Food = 'food',
	Medication = 'medication',
	Ammunition = 'ammunition'
}

export enum TradeStatus {
	Success = 'Success',
	Failed = 'Failed'
}