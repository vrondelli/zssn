import { Items } from '.';
import { Guid } from '@cashfarm/lang';

export interface IItem {
  name: Items;
  amount: number;
}

export interface IOffer {
  survivorId: string;
  items: IItem[];
}

export interface OfferItem {
  item: Items;
  amount: number;
}
