import { Guid } from '@cashfarm/lang';
import { provide } from '@cashfarm/plow';
import { MysqlStore, Table, TableName, StringField, PK, BooleanField, DateField, TableClass, DtoClass, NumberField, IOkResult } from '@cashfarm/store';

import { ConnectionPool } from '../db';
import { Inventory } from '../domain/entities/inventory';
import { inject } from 'inversify';
import { ItemStore, ItemDto } from './itemStore';
import { InventorySlot } from '../domain/valueObjects/inventorySlot';
import { IItem } from '../dsl/interfaces';


export class InventoryDto {
  public id: string;
  public survivorId: string;
  public updatedAt: Date;

  public async getAggregateFromDto() {
    const itemStore = new ItemStore()
    const itens = await itemStore.findAllByInventoryId(new Guid(this.id));

    const inventory = new Inventory(new Guid(this.survivorId), itens, new Guid(this.id));

    return inventory;
  }


  public loadFromAggregate(inventory: Inventory) {
    this.id = inventory.id.toString();
    this.survivorId = inventory.survivorId.toString();

    return this;
  }
}

@TableName('inventories')
export class InventoryTable extends Table {
  @PK()
  public id = new StringField('id');
  public survivorId = new StringField('survivorId');
  public updatedAt = new DateField('updatedAt');
}

@DtoClass(InventoryDto)
@TableClass(InventoryTable)
@provide(InventoryStore)
export class InventoryStore extends MysqlStore<InventoryTable, InventoryDto> {
  public itensStore = new ItemStore();

  constructor() {
    super(ConnectionPool);
  }

  public async findBySurvivorId(survivorId: Guid) {
    const inventoryDto = await this.findOne(q => q.where(t => t.survivorId.equals(survivorId.toString())));
    const itensStore = new ItemStore();
    const itens = await itensStore.findAllByInventoryId(new Guid(inventoryDto.id));

    const inventory = new Inventory(new Guid(inventoryDto.survivorId), itens, new Guid(inventoryDto.id));

    return inventory;
  }

  public async save(inventory: Inventory) {
    const inventoryDto = new InventoryDto().loadFromAggregate(inventory)
    
    inventory.slots.forEach(s => {
      const itemDto = new ItemDto();
      
      itemDto.id = new Guid().toString();
      itemDto.survivorId = inventory.survivorId.toString();
      itemDto.inventoryId = inventory.id.toString();
      itemDto.name = s.item;
      itemDto.amount = s.amount;

      this.itensStore.save(itemDto);
    });
    return super.create(inventoryDto);
  }

  public update(inventory: Inventory) {
    inventory.slots.forEach(s => {
      const itemDto = new ItemDto();

      itemDto.inventoryId = inventory.id.toString();
      itemDto.name = s.item;
      itemDto.amount = s.amount;

      this.itensStore.updateAmount(itemDto);
    })
    
    return super.update({ updatedAt: new Date() }, q => q.where(t => t.survivorId.equals(inventory.survivorId.toString())));
  } 
}