import { Guid } from '@cashfarm/lang';
import { provide } from '@cashfarm/plow';
import { MysqlStore, Table, TableName, StringField, PK, BooleanField, DateField, TableClass, DtoClass, NumberField, IOkResult } from '@cashfarm/store';

import { ConnectionPool } from '../db';
import { Items, IItem } from '../dsl';
import { SurvivorStore } from '.';

export class ItemDto {
  public id: string;
  public survivorId: string;
  public inventoryId: string;
  public name: Items;
  public amount: number;
  public updateAt: Date;
}

@TableName('items')
export class ItensTable extends Table {
  @PK()
  public id =  new StringField('id');
  public survivorId = new StringField('survivorId');
  public inventoryId = new StringField('inventoryId');
  public name = new StringField('name');
  public amount = new NumberField('amount');
  public updatedAt = new DateField('updatedAt');
}

@DtoClass(ItemDto)
@TableClass(ItensTable)
@provide(ItemStore)
export class ItemStore extends MysqlStore<ItensTable, ItemDto> {
  
  constructor() {
    super(ConnectionPool);
  }

  public async findAllByInventoryId(inventoryId: Guid) {
    const itemDto = await this.find(q => q.where(t => t.inventoryId.equals(inventoryId.toString())));

    const itens: IItem[] = itemDto.map(i => {
      const item: IItem = {
        name: i.name,
        amount: i.amount
      }

      return item;
    });
    
    return itens;
  }

  public async findAllBySurvivorId(survivorId: Guid) {
    const itemDto = await this.find(q => q.where(t => t.survivorId.equals(survivorId.toString())));

    const itens: IItem[] = itemDto.map(i => {
      const item: IItem = {
        name: i.name,
        amount: i.amount
      }

      return item;
    });
    
    return itens;
  }

  public async save(itemDto: ItemDto) {
    return super.create(itemDto);
  }

  public updateAmount(itemDto: ItemDto) {
    return this.update({amount: itemDto.amount, updatedAt: new Date()}, q => q.whereAll(t => t.inventoryId.equals(itemDto.inventoryId.toString()), t => t.name.equals(itemDto.name)));
  }

  public async computeItemAmount(item: Items) {
    const items = await this.find(q => q.where(t => t.name.equals(item)));
    const inicialValue = 0;
    const amount = items.reduce((a, b) => a + b.amount, 0)

    return amount;
  }

  public async getTotalPointsLost(valueMap: object) {
    const survivorStore = new SurvivorStore();

    const survivorIds = await survivorStore.getAllIfectedIds();
    const items = await this.find(q => q.where(t => t.survivorId.in(survivorIds)));

    const total = items.reduce((points, it) => points + (it.amount * valueMap[it.name]), 0);

    return total;
  }
}
