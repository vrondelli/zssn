import { Guid } from '@cashfarm/lang';
import { provide } from '@cashfarm/plow';
import { MysqlStore, Table, TableName, StringField, PK, BooleanField, DateField, TableClass, DtoClass, NumberField, IOkResult } from '@cashfarm/store';

import { ConnectionPool } from '../db';
import { Genders } from '../dsl';
import { Location } from '../domain/valueObjects';
import { Survivor } from '../domain/entities';
import { IItem } from '../dsl/interfaces';
import { InventoryStore, InventoryDto, ItemStore } from '.';
import { SSL_OP_ALL } from 'constants';

export class SurvivorDto {
  public id: string;
  public inventoryId: string;
  public name: string;
  public age: number;
  public gender: Genders;
  public latitude: string;
  public longitude: string;
  public firstSurvivorWhoFlagged: string;
  public secondSurvivorWhoFlagged: string;
  public thirdSurvivorWhoFlagged: string;
  public infected: boolean;
  public createdAt: Date;
  public updatedAt: Date;

  public loadFromAggregate(survivor: Survivor) {
    this.id = survivor.id.toString();
    this.inventoryId = survivor.inventoryId.toString();
    this.name = survivor.name;
    this.age = survivor.age;
    this.gender = survivor.gender;
    this.latitude = survivor.lastLocation.latitude;
    this.longitude = survivor.lastLocation.longitude;
    this.firstSurvivorWhoFlagged = survivor.survivorsWhoFlaggedAsInfected[0] ? survivor.survivorsWhoFlaggedAsInfected[0].toString() : null;
    this.secondSurvivorWhoFlagged = survivor.survivorsWhoFlaggedAsInfected[1] ? survivor.survivorsWhoFlaggedAsInfected[1].toString() : null;
    this.thirdSurvivorWhoFlagged = survivor.survivorsWhoFlaggedAsInfected[2] ? survivor.survivorsWhoFlaggedAsInfected[2].toString() : null;
    this.infected = survivor.infected;
    this.createdAt = survivor.createdAt;
    this.updatedAt = survivor.updateAt;

    return this;
  }

}

@TableName('survivors')
export class SurvivorsTable extends Table {
  @PK()
  public id = new StringField('id');
  public inventoryId = new StringField('inventoryId')
  public name = new StringField('name');
  public age = new NumberField('age');
  public gender = new StringField('gender');
  public latitude = new StringField('latitude');
  public longitude = new StringField('longitude');
  public firstSurvivorWhoFlagged = new StringField('firstSurvivorWhoFlagged');
  public secondSurvivorWhoFlagged = new StringField('secondSurvivorWhoFlagged');
  public thirdSurvivorWhoFlagged = new StringField('thirdSurvivorWhoFlagged');
  public infected = new BooleanField('infected');
  public createdAt = new DateField('createdAt');
  public updatedAt = new DateField('updatedAt');
}

@DtoClass(SurvivorDto)
@TableClass(SurvivorsTable)
@provide(SurvivorStore)
export class SurvivorStore extends MysqlStore<SurvivorsTable, SurvivorDto> {
  private inventories = new InventoryStore();
  private items = new ItemStore();

  constructor() {
    super(ConnectionPool);
  }

  public async findById(id: Guid) {
    const survivorDto = await this.findOne(q => q.where(t => t.id.equals(id.toString())))
    if (!survivorDto) return null;

    const timesFlaggedAsInfected = [];
    if (survivorDto.firstSurvivorWhoFlagged) timesFlaggedAsInfected.push(new Guid(survivorDto.firstSurvivorWhoFlagged));
    if (survivorDto.secondSurvivorWhoFlagged) timesFlaggedAsInfected.push(new Guid(survivorDto.secondSurvivorWhoFlagged));
    if (survivorDto.thirdSurvivorWhoFlagged) timesFlaggedAsInfected.push(new Guid(survivorDto.thirdSurvivorWhoFlagged));

    const items = await this.items.findAllBySurvivorId(new Guid(survivorDto.id));

    const survivor = new Survivor(
      survivorDto.name, survivorDto.age, survivorDto.gender,
      new Location(survivorDto.latitude, survivorDto.longitude),
      items, new Guid(survivorDto.id), new Guid(survivorDto.inventoryId),
      survivorDto.infected, timesFlaggedAsInfected, survivorDto.createdAt, survivorDto.updatedAt
    )

    return survivor;

  }

  public save(survivor: Survivor) {
    const survivorDto = new SurvivorDto().loadFromAggregate(survivor);
    this.inventories.save(survivor.inventory);

    return super.create(survivorDto);
  }

  public updateSurvivor(survivor: Survivor) {
    const survivorDto = new SurvivorDto().loadFromAggregate(survivor);

    return this.update(survivorDto, q => q.where(t => t.id.equals(survivorDto.id.toString())
    ));
  }

  public updateInventory(survivor: Survivor) {
    const survivorDto = new SurvivorDto().loadFromAggregate(survivor);

    return this.inventories.update(survivor.inventory);
  }

  public computeAllSurvivors() {
    return this.compute<number>('COUNT(ID)');
  }

  public computeNonInfectedSurvivors() {
    return this.compute<number>(q =>
      q.select(t => [new NumberField('COUNT(id)')])
        .where(t => t.infected.isFalse())
    );
  }

  public computeInfectedSurvivors() {
    return this.compute<number>(q =>
      q.select(t => [new NumberField('COUNT(id)')])
        .where(t => t.infected.isTrue())
    );
  }

  public async getAllIfectedIds() {
    const survivorIds = (await this.find(q => q.where(t => t.infected.isTrue()))).map(dto => dto.id)
    
    return survivorIds;
  }
}
