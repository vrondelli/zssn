import * as Hapi from 'hapi';
import * as Joi from 'joi';
import { Request } from '@cashfarm/tractor';
import { Genders, Items } from '../../dsl';
import { Location } from '../../domain/valueObjects';
import { IItem } from '../../dsl/interfaces';

export class CreateSurvivorRequest extends Request {
	public static schema = {
		payload: Joi.object({
			name: Joi.string().required(),
			age: Joi.number().required(),
			gender: Joi.string().only(Genders.Female, Genders.Male).required(),
			location: Joi.object({
				latitude: Joi.string().required(),
				longitude: Joi.string().required()
			}).required(),
			itens: Joi.array().items(Joi.object({
				name: Joi.string().only(Items.Water, Items.Food, Items.Medication, Items.Ammunition).required(),
				amount: Joi.number().required()
			})).required()
		})
	};

	public readonly name: string;
	public readonly age: number;
	public readonly gender: Genders;
	public readonly location: Location;
	public readonly itens: IItem[];

	constructor(req: Hapi.Request) {
		super(req.params, req.query, req.payload);

		this.deserializeData();

		this.location = new Location(req.payload.location.latitude, req.payload.location.longitude);
	}
}