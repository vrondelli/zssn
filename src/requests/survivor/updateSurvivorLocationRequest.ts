import * as Hapi from 'hapi';
import * as Joi from 'joi';
import { Request } from '@cashfarm/tractor';
import { Genders, Items } from '../../dsl';
import { Location } from '../../domain/valueObjects';
import { IItem } from '../../dsl/interfaces';
import { Guid } from '@cashfarm/lang';

export class UpdateLocationRequest extends Request {
  public static schema = {
    params: {
      id: Joi.string().guid()
    },
    payload: Joi.object({
      location: Joi.object({
        latitude: Joi.string().required(),
        longitude: Joi.string().required()
      }).required()
    })
  };
  
  public readonly id: Guid;
  public readonly location: Location;

  constructor(req: Hapi.Request) {
    super(req.params, req.query, req.payload);
    
    this.id = new Guid(req.params.id);
    this.location = new Location(req.payload.location.latitude, req.payload.location.longitude);
  }
}