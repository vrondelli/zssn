import * as Hapi from 'hapi';
import * as Joi from 'joi';
import { Request } from '@cashfarm/tractor';
import { Items, IOffer } from '../../dsl';
import { Guid } from '@cashfarm/lang';

export class TradeRequest extends Request {
	public static schema = {
		payload: Joi.object({
			offers: Joi.array().items(Joi.object({
				survivorId: Joi.string().guid().required(),
        items: Joi.array().items(Joi.object({
					name: Joi.string().only(Items.Ammunition, Items.Water, Items.Medication, Items.Food).required(),
					amount: Joi.number().required()
				})).max(2).required()
			})).required()
		})
	};

  public readonly offers: IOffer[]; 

	constructor(req: Hapi.Request) {
		super(req.params, req.query, req.payload);

		this.deserializeData()		
	}
}