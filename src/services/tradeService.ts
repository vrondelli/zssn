import { provide } from '@cashfarm/plow';
import { Items, TradeStatus } from '../dsl';
import { IOffer } from '../dsl/interfaces';
import { inject, multiInject } from 'inversify';
import { SurvivorStore } from '../readModel';
import { Guid } from '@cashfarm/lang';
import { Offer } from '../domain/valueObjects/offer';
import { Survivor } from '../domain/entities';

export class TradeResponse {
	constructor(
		public status: TradeStatus,
		public messsage: string
	) { }
}

@provide(TradeService)
export class TradeService {
	public ValueMap() {
		const map = {};

		map[Items.Water] = 4;
		map[Items.Food] = 3;
		map[Items.Medication] = 2;
		map[Items.Ammunition] = 1;

		return map;
	}

	private survivorStore = new SurvivorStore();

	constructor() { }

	public async trade(firstSurvivor: Survivor, secondSurvivor: Survivor, offers: IOffer[]) {
		const firstSurvivorOffer = new Offer(offers[0], this.ValueMap());
		const secondSurvivorOffer = new Offer(offers[1], this.ValueMap());

		if (firstSurvivorOffer.totalPoints > secondSurvivorOffer.totalPoints) {
			return new TradeResponse(TradeStatus.Failed, `Survivor offer with ${secondSurvivor.id.toString()} id does not have enough points amount`);
		}
			
		if (firstSurvivorOffer.totalPoints < secondSurvivorOffer.totalPoints) {
			return new TradeResponse(TradeStatus.Failed, `Survivor offer with ${firstSurvivor.id.toString()} id does not have enough points amount`);
		}

		const firstSurvivorTrade = firstSurvivor.tradeItens(firstSurvivorOffer, secondSurvivorOffer);

		if (firstSurvivorTrade.status === TradeStatus.Success) {
			const secondSurvivorTrade = secondSurvivor.tradeItens(secondSurvivorOffer, firstSurvivorOffer);

			if (secondSurvivorTrade.status === TradeStatus.Success) {
				this.survivorStore.updateInventory(firstSurvivor);
				this.survivorStore.updateInventory(secondSurvivor);

				return secondSurvivorTrade;
			} 
			else {
				return secondSurvivorTrade;
			}
		} else {
			return firstSurvivorTrade;
		}
	}
}